# Gramma

**Recipe Manager**

Gramma is a basic, text based, recipe tracking application that uses the
`cmd.Cmd` framework. Users can create recipes, ingredients, instrunction sets
and bring them together under one Gramma's collection. Recipes can also be
categorized under deserts, main courses, by ingredient types like chicken,
beef, soups and much more.

