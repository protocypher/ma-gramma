from setuptools import setup


setup(
      name="Gramma",
      version="0.1.0",
      packages=["gramma"],
      url="https://bitbucket.org/protocypher/ma-gramma",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A recipe management and execution application."
)

